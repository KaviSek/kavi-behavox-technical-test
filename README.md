
#  Enron Email Classifier

<span> This notebook details a simple TFIDF and SGD machine learing pipeline that can be used to classify an email as a business or personal email given the corpus from the email's subject body. The final pipeline achieves a ROC_AUC score of **0.999** on the training data and **0.988** on the test data during cross validation. The pipeline scored **0.958** on a secondary holdout data set afterwards.</span>

### Algorithims
**Count Vectorizer** - Product a Sparse Representation of Word Counts within a corpus
<br>**TFIDF Matrix** - reflects important words within a document by comparing term frequency across the number of documents. 
<br>**Scholastics Gradient Descent** - Stochastic Gradient Descent (SGD) is a simple yet very efficient approach to discriminative learning of linear classifiers under convex loss functions. Works well with the sparse TFIDF Matrix creating in the preprocessing of our machine learing pipeline

### Classification Metrics

Metrics: AUC_ROC, F1_Score, Recall, Precision, Accuracy

The recall is the number of correct results divided by the total number of results. Precision, on the other hand, tells us how many selected items are relevant. Maximizing precision or recall is relatively simple so I also measure the hyperbolic mean between these scores, which would be the F1 scores. Our main metrics will be the Area under the ROC and AUC curve. The ROC is a plot to measure the difference between the TP rate and the FP rate. A higher ROC curve with a larger area under the cure is what I am looking for. I have included the overall accuracy scores within my evaluation as well for reference.

### Import Preliminaries


```python
%matplotlib inline
%config InlineBackend.figure_format = 'retina'

# Import Modules
import os
import itertools
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import warnings
import seaborn as sns

# Addititional Import
from matplotlib import rcParams, gridspec
from sklearn.feature_extraction.text import TfidfTransformer, CountVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import (confusion_matrix, precision_recall_curve, auc,
                             roc_curve, recall_score, classification_report,
                             f1_score, precision_score, recall_score,
                             precision_recall_fscore_support, roc_auc_score)
from sklearn.model_selection import cross_validate, GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.utils.class_weight import compute_sample_weight

# Warning Configuration
warnings.filterwarnings('ignore')
```

### Functions


```python
# Grid search groupby function that aggregate grid search data
def grid_search_groupby(results: pd.DataFrame, param_1: str, param_2: str) -> pd.DataFrame:
    '''
    Create a aggregated dataframe from the grid search results use the two
    hyper paramters that we pass into the function. We will be using this
    function to plot heatmaps from our grid search.
    
    Parameters
    ----------
    results: DataFrame of Grid Score results.
    
    Examples
    ----------
    >>> (grid_search_groupby(results,'max_depth','n_estimators')
    >>> grid_search_groupby(results,'max_leaf_nodes','n_estimators')
    '''
    assert (type(results) ==  type(pd.DataFrame())), 'results should be a pandas.core.frame.DataFrame'
    assert (type(param_1) == str), 'param_1 should be a string'
    assert (type(param_2) == str), 'param_2 should be a string'
    
    params_df  = pd.DataFrame.from_dict(list(results.params.values))
    mean_test_score = results.mean_test_score
    result_shrt_df = pd.concat([mean_test_score, params_df], axis=1)
    result_groupby = result_shrt_df.groupby([param_1, param_2])['mean_test_score'].mean().unstack()
    return result_groupby

# Plot a confusion matrix with matplotlib given a confusion matrix
def plot_confusion_matrix(cm, classes, fontsize=20,
                          normalize=False, title='Confusion matrix',
                          cmap=plt.cm.Blues):
    '''
    THE MAIN CONFUSION MATRIX, KAVI DON'T DELTETE BY ACCIDENT AGAIN. Function plots a 
    confusion matrix given a cm matrix and class names

    Parameters
    ----------
    cm: sklearn confusion matrix
    classes: numpy 1D array containing all unique class names

    Examples
    ---------
    >>>>

    plot_confusion_matrix(
    cm,
    classes,
    fontsize=25,
    normalize=True,
    title=model.name.capitalize() + ': Test Set',
    cmap=plt.cm.Greens)

    '''
    cm_num = cm
    cm_per = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        # print("Normalized confusion matrix")
    else:
        None
        # print('Confusion matrix, without normalization')

    # print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title.replace('_',' ').title()+'\n', size=fontsize)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45, size=fontsize)
    plt.yticks(tick_marks, classes, size=fontsize)

    fmt = '.5f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        # Set color parameters
        color = "white" if cm[i, j] > thresh else "black"
        alignment = "center"

        # Plot perentage
        text = format(cm_per[i, j], '.5f')
        text = text + '%'
        plt.text(j, i,
            text,
            fontsize=fontsize,
            verticalalignment='baseline',
            horizontalalignment='center',
            color=color)
        # Plot numeric
        text = format(cm_num[i, j], 'd')
        text = '\n \n' + text
        plt.text(j, i,
            text,
            fontsize=fontsize,
            verticalalignment='center',
            horizontalalignment='center',
            color=color)

    plt.tight_layout()
    plt.ylabel('True label'.title(), size=fontsize)
    plt.xlabel('Predicted label'.title(), size=fontsize)

    return None

# Confusion matrix plotting function or normal classifier
def plot_confusion_normal(model, classes, name, train_x, train_y,
                          test_x, test_y, cmap=plt.cm.Greens):
    '''
    Fuction plota grid and calls the plot_confusion_matrix function
    to plot two confusion matrices. One for the tarin set and another
    for the test set

    Parameters
    ----------
    cm: sklearn confusion matrix
    classes: numpy 1D array containing all unique class names

    Examples
    ----------
    >>>> plot_confusion_normal(xg_model, train_x, train_y)
    >>>> plot_confusion_normal(rf_model, train_x, train_y)
    '''

    # Set the plot size
    rcParams['figure.figsize'] = (30.0, 22.5)

    # Set up grid
    plt.figure()
    fig = gridspec.GridSpec(3, 3)
    grid_length = list(range(1, 3))
    tuple_grid = [(i, j) for i in grid_length for j in grid_length]

    # Plot Training Confusion Matrix
    plt.subplot2grid((3, 3), (0, 0))
    cm = confusion_matrix(train_y, model.predict(train_x))
    plot_confusion_matrix(
        cm,
        classes=classes,
        normalize=True,
        title=name.capitalize() + ': Train Set',
        cmap=cmap)

    # Plot Testing Confusion Matrix
    plt.subplot2grid((3, 3), (0, 1))
    cm = confusion_matrix(test_y, model.predict(test_x))
    plot_confusion_matrix(
        cm,
        classes=classes,
        normalize=True,
        title=name.capitalize() + ': Test Set',
        cmap=cmap)

    return None        

# Retrieve email names
def retrieve_email_names(path:str) -> list:
    '''
    This function with search the provided file path and return a
    list of all the files names within path
    
    Parameters
    ----------
    path: a string input specifying the file path
    
    Examples
    ----------
    >>>> retrieve_email_names/kavi/emails/business')
    >>>> retrieve_email_names(/kavi/emails/personal')
    '''
    
    email_names = []
    for root, dirs, files in os.walk(path):
        for file in files:
            email_names.append(file)
    return email_names


# Retriveve email content from directory
def retrieve_email_content(path):
    '''
    This function with search the provided file path and return a 
    list of corpus for all the files witin the directory. Note that
    this function will filter the emails for only the subject lines
    and will note return the email metadata from within this function.
    
    Parameters
    ----------
    path: a string input specifying the file path
    
    Examples
    ---------
    >>>> retrieve_email_content(/kavi/emails/business')
    >>>> retrieve_email_content(/kavi/emails/personal')
    
    '''
    email_content = []
    for root, dirs, files in os.walk(path):
        for file in files:
            file_path = path+'/'+file
            with open(file_path,  encoding='windows-1252') as email_file:
                email = email_file.read()
                start_pos = email.find('\n\n')
                end_pos = email.find('<MARKUP id')
                subject = email[start_pos:end_pos]
                email_content.append(subject)
    return email_content

```

### Import Data

Aggregate all the emails from the business and personal sub directories. Strip any information before the first new line character and everythin after the first `<<MARKUP id=alex...>` HTML tag. When we import the we filter the string for the only the information within the body. Next I strip any unecassary information from the beforing aggregating the data into our training dataset.


```python
# Local File Path to business and personal email data
business = 'Data/business'
personal = 'Data/personal'

# Generate a list of files
business_email_names = retrieve_email_names(business)
personal_email_names = retrieve_email_names(personal)
business_email_content = retrieve_email_content(business)
personal_email_content = retrieve_email_content(personal)

# Print the length of the returned lists
print('Business Email Name Length:', len(business_email_names))
print('Business Email Concent Length:', len(business_email_content))
print('Personal Email Name Length', len(personal_email_names))
print('Personal EMail Coentent Length', len(personal_email_content))
```

    Business Email Name Length: 4871
    Business Email Concent Length: 4871
    Personal Email Name Length 1855
    Personal EMail Coentent Length 1855



```python
# Appending List Data into a Single dataframe
# Targt Values: Business Email = 0, Personal Email = 1
email_target = [0]*len(business_email_names) + [1]*len(personal_email_names)
email_names = business_email_names + personal_email_names
email_content = business_email_content + personal_email_content

# Let Combiine our data into a single dataset
data = pd.DataFrame(data={
    'name': email_names,
    'content': email_content,
    'target': email_target
})

# View the head of the dataframe
data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>name</th>
      <th>content</th>
      <th>target</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>maildir_cash-m_inbox_287</td>
      <td>\n\nMichelle - can you respond?  I haven't see...</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>maildir_guzman-m_notes_inbox_916</td>
      <td>\n\nHey Guys.\n\nBe cautious when doing a NP/M...</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>maildir_taylor-m_notes_inbox_1683</td>
      <td>\n\nMark:\n\nHere is a draft of what I propose...</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>maildir_sanders-r_notes_inbox_182</td>
      <td>\n\nAttached is an initial draft of an Agreeme...</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>maildir_shackleton-s_notes_inbox_1975</td>
      <td>\n\nClint/Sara-\n\nI read the UBS &amp; CSFB confi...</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>



### Build Pipelines


```python
# Encode our data into numpy arrays 
X = data.content.values
y = data.target.values

# Train-test split the data
X, X_holdout, y, y_holdout = train_test_split(X,y, test_size=0.15)
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.25)

# Compute sample weights
weights = compute_sample_weight(class_weight='balanced', y=y_train)
```


```python
# Building a simple for the model pipeline 
sgd_pipeline = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('classifier', SGDClassifier(loss='hinge', penalty='l2',
                          alpha=1e-3, random_state=42,
                          max_iter=5, tol=None)),], )
```

### Pipeline Evaluation


```python
# Fit our pipelinee
sgd_pipeline.fit(X_train,y_train, **{'classifier__sample_weight':weights})
sgd_pred = sgd_pipeline.predict(X_train)
```


```python
# Calculate the cross validation score of the pipeline
scores = cross_validate(sgd_pipeline, X_train, y_train, 
                         cv=10, 
                         fit_params={'classifier__sample_weight':weights},
                         scoring=['accuracy','precision','recall','f1','roc_auc'],)

# Cross validation scores
scores_df = pd.DataFrame(scores).T
scores_df['Mean'] = scores_df.mean(axis=1)
scores_df['Mean']
```




    fit_time           0.653744
    score_time         0.354003
    test_accuracy      0.952648
    train_accuracy     0.972164
    test_precision     0.888500
    train_precision    0.920810
    test_recall        0.947458
    train_recall       0.983522
    test_f1            0.916827
    train_f1           0.951111
    test_roc_auc       0.985384
    train_roc_auc      0.995484
    Name: Mean, dtype: float64



##### Confusion Matrix


```python
# Plot confusion matrix
plot_confusion_normal(model=sgd_pipeline, classes=[0,1], 
                      name = 'Pipeline',
                      train_x=X_train,
                      test_x=X_test,
                      train_y=y_train, test_y=y_test,
                      cmap= plt.cm.Purples)
```


![png](output_14_0.png)


##### Pipeline Grid Search


```python
# Setting up the grid
grid = {'classifier__penalty': ['l2','l1','elasticnet',],
        'classifier__loss': ['hinge', 'log', 'modified_huber', 
                             'squared_hinge', 'perceptron'],
        'classifier__alpha':[0.0000001,0.000001,0.00001,
                                0.0001,0.001,0.01,0.1,1]}

# Initialize with GridSearchCV with grid
grid_search = GridSearchCV(estimator=sgd_pipeline, param_grid=grid, 
                     scoring='roc_auc', n_jobs=-1, refit=True, cv=10,
                     return_train_score=True, verbose=0)

# Fit search
grid_search.fit(X,y);
```


```python
# Print the best grid search score
print('Accuracy of best parameters: %.5f'%grid_search.best_score_)
print('Best parameters: %s' %grid_search.best_params_)
```

    Accuracy of best parameters: 0.98833
    Best parameters: {'classifier__alpha': 1e-05, 'classifier__loss': 'log', 'classifier__penalty': 'l2'}



```python
# Plot Grid Seach Results between penalty and loss
results = pd.DataFrame(grid_search.cv_results_)
result_groupby_1 = grid_search_groupby(results,
                                       'classifier__penalty',
                                       'classifier__loss')

rcParams['figure.figsize'] = (8.0, 7.0)
plt.figure()
sns.heatmap(grid_search_groupby(results,'classifier__penalty',
                                'classifier__loss'),
           cmap=plt.cm.Purples, annot=True, fmt='.4f');
plt.title('Average Grid Search Result: Penalty vs Loss ');
plt.xlabel('Loss')
plt.ylabel('Penalty');


# Plot grid search resutlts between alpha and penalty
results = pd.DataFrame(grid_search.cv_results_)
result_groupby_1 = grid_search_groupby(results,
                                       'classifier__penalty',
                                       'classifier__alpha')

plt.figure()
sns.heatmap(grid_search_groupby(results,'classifier__penalty',
                                'classifier__alpha'),
           cmap=plt.cm.Purples, annot=True, fmt='.4f');
plt.title('Average Grid Search Result: Penalty vs Alpha ');
plt.xlabel('Alpha')
plt.ylabel('Penalty');


# Plot grid search resutlts between loss and alpha
results = pd.DataFrame(grid_search.cv_results_)
result_groupby_1 = grid_search_groupby(results,
                                       'classifier__loss',
                                       'classifier__alpha')

plt.figure()
sns.heatmap(grid_search_groupby(results,'classifier__loss',
                                'classifier__alpha'),
           cmap=plt.cm.Purples, annot=True, fmt='.4f',);
plt.title('Average Grid Search Result: Loss vs Alpha ');
plt.xlabel('Alpha')
plt.ylabel('Loss')
plt.yticks(rotation=90);
```


![png](output_18_0.png)



![png](output_18_1.png)



![png](output_18_2.png)


### Final Pipeline


```python
# Print the best model parameters
print('Best parameters: %s' %grid_search.best_params_)
```

    Best parameters: {'classifier__alpha': 1e-05, 'classifier__loss': 'log', 'classifier__penalty': 'l2'}



```python
# Building a simple for the model pipeline 
sgd_pipeline = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('classifier', SGDClassifier(loss='hinge', penalty='l2',
                          alpha=0.0001, random_state=42,
                          max_iter=5, tol=None)),], )

# Compute the cross-validation score for the best model again
scores = cross_validate(sgd_pipeline, X_train, y_train, 
                         cv=10, 
                         fit_params={'classifier__sample_weight':weights},
                         scoring=['accuracy','precision','recall','f1','roc_auc'],)

# Cross validation scores
scores_df = pd.DataFrame(scores).T
scores_df['Mean'] = scores_df.mean(axis=1)
scores_df['Mean']
```




    fit_time           0.670470
    score_time         0.362821
    test_accuracy      0.954978
    train_accuracy     0.996372
    test_precision     0.904063
    train_precision    0.987178
    test_recall        0.937288
    train_recall       0.999812
    test_f1            0.919889
    train_f1           0.993453
    test_roc_auc       0.985550
    train_roc_auc      0.999896
    Name: Mean, dtype: float64




```python
# Fit our final model 
sgd_pipeline.fit(X_train,y_train, **{'classifier__sample_weight':weights})

# Plot confusion matrix
plot_confusion_normal(model=sgd_pipeline, classes=[0,1], 
                      name = 'Pipeline',
                      train_x=X_train,
                      test_x=X_test,
                      train_y=y_train, test_y=y_test,
                      cmap= plt.cm.Purples)
```


![png](output_22_0.png)


##### Holdout Score


```python
# ROC_AUC score of secondary holdout set
roc_auc_score(y_holdout, sgd_pipeline.predict(X_holdout))
```




    0.9583525967021114



Author: Kavi Sekhon
